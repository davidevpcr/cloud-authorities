<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cloud_authorities_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+4R5Nsb0xS-<8kS)c<qb~9,WF*NW{e;4gTb;s&jP75dA}~p-`yxKXcM:1n p?~>i');
define('SECURE_AUTH_KEY',  'wjxNnb4 B~;<2Kq/l(SdmBrpX;,@cyCu/ZTkS>8hmor/Z_J?#/rIwY~g4]W,g>l)');
define('LOGGED_IN_KEY',    ']-i{rDekR2+dVsX9E}m6ps#(|F<[l(oVCWz*-z^B>*?F(1vnwifkA9udb 0j)AS8');
define('NONCE_KEY',        'N9UXpVuwq[M(C|25q8`Vvl,@T3Zr2@(~7rJdoBW1eG?Yoa; [c(L:Z6F`==+QHud');
define('AUTH_SALT',        '@m]AM?pcH%i;+5u!K#qb(Ku$}q0tU+N|5~L?JW};,CAy9IS;}oj `QGf8B2n?$bj');
define('SECURE_AUTH_SALT', 'UP$ZoS.wm/z%.ePY<>v01-mtyF#T H>+S5(>%[1YsT#pZEE20#sX5(|?OW+ki2GC');
define('LOGGED_IN_SALT',   'cx4!N63&ULu%%{x+99uG_o,:7|A$rwf5D/JdU^6v`oMaFJ]~miS9Y{I+/Vl5hUKJ');
define('NONCE_SALT',       '3buYfy; !Y8zs,wtsAaC`Z[8ag+x1uj1]I>U^3(6n]U=tm-rQp5)]7I{r!];L_>t');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
