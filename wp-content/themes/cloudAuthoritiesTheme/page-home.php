<?php get_header(); ?>

<?php while(have_posts()): the_post(); ?>

	<header class="header page-section">
		<div class="container">
			<div class="top-menu">
				<div class="logo">
					<a href="<?php echo esc_url(home_url('/')); ?>">
						<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" >
					</a>
				</div> <!-- .logo -->
				
				<div class="mobile-menu">
					<a href="#" class="mobile"><i class="fa fa-bars" arial-hidden="true"></i> Menu</a>
				</div>

				<div class="header-menu">
					<?php $args = array(
						'theme_location' => 'header-menu',
						'container' => 'nav',
						'container_class' => 'header-menu-items',
						'container_id' => 'header-menu-items'
						);
						wp_nav_menu($args);
					?>
				</div> <!-- .header-info -->
			</div> <!-- .top-menu -->

			<div class="header-content">
				<div class="left">
					<?php the_content(); ?>
				</div>

				<div class="right">
					<?php echo do_shortcode( '[contact-form-7 id="59" title="Contact form 1"]' ); ?>
				</div>

			</div>
		</div> <!-- .container -->
	</header>


	<div class="services-home page-section">
		<div class="container">
			<div class="service-container">
				<?php
					$id_image = get_field('icon_01');
					$image = wp_get_attachment_image_src($id_image);
				?>
				<img src="<?php echo $image[0];?>" class="service-icon" >

				<div class="service-description">
					<?php the_field('description_1'); ?>
				</div>
			</div> <!-- service 1 -->

			<div class="service-container">
				<?php
					$id_image = get_field('icon_02');
					$image = wp_get_attachment_image_src($id_image);
				?>
				<img src="<?php echo $image[0];?>" class="service-icon" >

				<div class="service-description">
					<?php the_field('description_2'); ?>
				</div>
			</div> <!-- service 2 -->

			<div class="service-container">
				<?php
					$id_image = get_field('icon_03');
					$image = wp_get_attachment_image_src($id_image);
				?>
				<img src="<?php echo $image[0];?>" class="service-icon" >

				<div class="service-description">
					<?php the_field('description_3'); ?>
				</div>
			</div> <!-- service 3 -->

			<div class="service-container">
				<?php
					$id_image = get_field('icon_04');
					$image = wp_get_attachment_image_src($id_image);
				?>
				<img src="<?php echo $image[0];?>" class="service-icon" >

				<div class="service-description">
					<?php the_field('description_4'); ?>
				</div>
			</div> <!-- service 4 -->
		</div> <!-- .container -->
	</div> <!-- .services-home -->

	<div class="trust-section page-section">
		<div class="container">
			<div class="trust-description">
					<?php the_field('carriers_description'); ?>
			</div>
			<div class="trust-logos">
				<?php
					$c = 1;
					while($c <= 6) {
						$id_image = get_field('carrier_logo_' . $c);
						$image = wp_get_attachment_image_src($id_image);
						$c = $c + 1;
				?>
					<img src="<?php echo $image[0];?>" class="trust-icon" >
				<?php } ?>
			</div> <!-- .trust-logos -->
		</div> <!-- .container -->
	</div> <!-- .trust-home -->

	<div class="testimonial-home page-section">
		<div class="container">
			<?php the_field('testimonial'); ?>
		</div>
	</div> <!-- .testimonial-home .container -->

	<div class="about-us-section page-section">

		<div class="info-box">
			<div class="container">
				<div class="about-us-description">
					<?php the_field('about_us'); ?>
				</div>
			</div> <!-- .about-us-section .container -->

			<div class="about-us-links">
				<?php
					the_field('about_us_link_1');
					the_field('about_us_link_2');
				?>
			</div> <!-- .about-us-section .about-us-links -->
		</div>
	</div>

	<div class="call-us-box page-section">
		<div class="container">
			<?php the_field('call_us'); ?>
			
		</div>
	</div>




<?php endwhile; ?>

<?php get_footer(); ?>
