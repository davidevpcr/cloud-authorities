<?php
/*
*	Template Name: Service Detailed
*/

get_header(); ?>

<?php while(have_posts()): the_post(); ?>
	<div class="services-detailed-page">
		<header class="header page-section">
			<div class="container">
				<div class="top-menu">
					<div class="logo">
						<a href="<?php echo esc_url(home_url('/')); ?>">
							<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" >
						</a>
					</div> <!-- .logo -->
					
					<div class="mobile-menu">
						<a href="#" class="mobile"><i class="fa fa-bars" arial-hidden="true"></i> Menu</a>
					</div>

					<div class="header-menu">
						<?php $args = array(
							'theme_location' => 'header-menu',
							'container' => 'nav',
							'container_class' => 'header-menu-items',
							'container_id' => 'header-menu-items'
							);
							wp_nav_menu($args);
						?>
					</div> <!-- .header-info -->
				</div> <!-- .top-menu -->

				<div class="title">
					<h1><?php the_title(); ?></h1>
				</div>

			</div> <!-- .container -->
		</header>

		<div class="general-description-section page-section">
			<div class="container">	
				<div class="general-description">
						<?php the_content(); ?>
				</div>
			</div>
		</div>

		<div class="services-detailed-section page-section">
			<div class="container">	
				<div class="services-detailed-title-section">
					<?php the_field('title_section_1'); ?>					
				</div>

				<div class="detail-container det1">
					<div class="left-box"> 
						<?php
							$id_image = get_field('service_detail_image_1');
							$image = wp_get_attachment_image_src($id_image);
						?>
						<img src="<?php echo $image[0];?>" class="detail-icon" >
					</div>

					<div class="right-box">
						<div class="service-title">
							<?php the_field('service_detail_title_1'); ?>
						</div>
						<div class="service-details">
							<?php the_field('service_detail_text_1'); ?>
						</div>
					</div> <!-- .right-box -->
				</div> <!-- .detail-container.serv1 -->
			</div> <!-- .services-detailed-section .container -->			
		</div> <!-- .services-detailed-section.page-section -->


		<div class="services-detailed-central-section page-section">
			<div class="container">	
				<div class="detail-container det2">
					<div class="mobile-style">
						<?php
							$id_image = get_field('service_detail_image_2');
							$image = wp_get_attachment_image_src($id_image);
						?>
						<img src="<?php echo $image[0];?>" class="detail-icon" >
					</div>

					<div class="left-box"> 
						<div class="service-title">
							<?php the_field('service_detail_title_2'); ?>
						</div>
						<div class="service-details">
							<?php the_field('service_detail_text_2'); ?>
						</div>
					</div>

					<div class="right-box">
						<img src="<?php echo $image[0];?>" class="detail-icon" >
					</div> <!-- .right-box -->
				</div> <!-- .detail-container.serv2 -->
			</div> <!-- .services-detailed-central-section .container -->			
		</div> <!-- .services-detailed-central-section.page-section -->

		<div class="services-detailed-section page-section">
			<div class="container">	
				<div class="detail-container det3">
					<div class="left-box"> 
						<?php
							$id_image = get_field('service_detail_image_3');
							$image = wp_get_attachment_image_src($id_image);
						?>
						<img src="<?php echo $image[0];?>" class="detail-icon" >
					</div>

					<div class="right-box">
						<div class="service-title">
							<?php the_field('service_detail_title_3'); ?>
						</div>
						<div class="service-details">
							<?php the_field('service_detail_text_3'); ?>
						</div>
					</div> <!-- .right-box -->
				</div> <!-- .detail-container.serv3 -->
			</div> <!-- .services-detailed-section .container -->			
		</div> <!-- .services-detailed-section.page-section -->

		<div class="second-title-section page-section">
			<div class="container">
				<div class="second-title">
					<?php the_field('title_section_2'); ?>
				</div>
			</div>
		</div>

		<div class="more-details-section page-section">
			<div class="container">
				<div class="more-details">

					<div class="mobile-style">
						<?php the_field('more_details_left'); ?>
						<?php the_field('more_details_right'); ?>
					</div>

					<div class="left-box"> 
						<?php the_field('more_details_left'); ?>
					</div> <!-- .more-details .left-box -->

					<div class="right-box">
						<?php the_field('more_details_right'); ?>
					</div> <!-- .more-details .right-box -->
				</div>
			</div>
		</div> <!-- .more-details-section.page-section -->


		<div class="trust-section page-section">
			<div class="container">
				<div class="trust-description">
					<?php the_field('carriers_description'); ?>
				</div>
				<div class="trust-logos">
					<?php
						$c = 1;
						while($c <= 6) {
							$id_image = get_field('carrier_logo_' . $c);
							$image = wp_get_attachment_image_src($id_image);
							$c = $c + 1;
					?>
						<img src="<?php echo $image[0];?>" class="trust-icon" >
					<?php } ?>
				</div> <!-- .trust-logos -->
			</div> <!-- .container -->
		</div> <!-- .trust-section -->


		<div class="about-us-links-section page-section">
			<div class="info-box"> 
				<div class="about-us-links">
					<?php
						the_field('about_us_link_1');
						the_field('about_us_link_2');
					?>
				</div> <!-- .info-box .about-us-links -->
			</div>
		</div> <!-- .about-us-title -->

		<div class="call-us-box page-section">
			<div class="container">
				<?php the_field('call_us'); ?>
				
			</div>
		</div>		

		<?php endwhile; ?>

		<?php  get_footer(); ?>

	</div> <!-- .services-detailed -->
	