<?php get_header(); ?>

<?php while(have_posts()): the_post(); ?>
	<div class="general-services-page">
		<header class="header page-section">
			<div class="container">
				<div class="top-menu">
					<div class="logo">
						<a href="<?php echo esc_url(home_url('/')); ?>">
							<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" >
						</a>
					</div> <!-- .logo -->
					
					<div class="mobile-menu">
						<a href="#" class="mobile"><i class="fa fa-bars" arial-hidden="true"></i> Menu</a>
					</div>

					<div class="header-menu">
						<?php $args = array(
							'theme_location' => 'header-menu',
							'container' => 'nav',
							'container_class' => 'header-menu-items',
							'container_id' => 'header-menu-items'
							);
							wp_nav_menu($args);
						?>
					</div> <!-- .header-info -->
				</div> <!-- .top-menu -->

				<div class="title">
					<h1>Services</h1>
				</div>

			</div> <!-- .container -->
		</header>

		<div class="general-services-section page-section">

			<div class="container">	

				<div class="service-container serv1">
					<div class="left-box">
						<img src="<?php echo get_template_directory_uri(); ?>/img/services_data.png" class="service-icon" >		
					</div>
					<div class="right-box">
						<div class="service-title">
							<?php the_field('service_title_1'); ?>
						</div>
						<div class="service-details">
							<?php the_field('service_details_1'); ?>
						</div>							
						
						<div class="items-list">
							<div class="left-list">										
								<?php the_field('left_list_items_1'); ?>
							</div>
							<div class="right-list">
								<?php the_field('right_list_items_1'); ?>									
							</div>

							<div class="one-list">				
								<?php the_field('left_list_items_1'); ?>
								<?php the_field('right_list_items_1'); ?>
							</div>							
						</div>
						<div class="read-more-link">
							<?php
								$posts = get_field('link_service_1');
								if( $posts ): 
									foreach( $posts as $post): ?>
										<a href="<?php the_permalink(); ?>">READ MORE > </a>
									<?php endforeach; 
									wp_reset_postdata();
								endif; ?>
						</div>

					</div> <!-- .right-box -->
				</div> <!-- .service-container.serv1 -->

				<div class="service-container serv2">
					<div class="mobile-style">
						<div class="img-box">
							<img src="<?php echo get_template_directory_uri(); ?>/img/services_security.png" class="service-icon">
						</div>
						<div class="service-title">
							<?php the_field('service_title_2'); ?>
						</div>
						<div class="service-details">
							<?php the_field('service_details_2'); ?>
						</div>

						<div class="items-list">
							<div class="one-list">				
								<?php the_field('left_list_items_2'); ?>
								<?php the_field('right_list_items_2'); ?>
							</div>
							<div class="left-list">										
								<?php the_field('left_list_items_2'); ?>
							</div>
							<div class="right-list">
								<?php the_field('right_list_items_2'); ?>									
							</div>
						</div>
						<div class="read-more-link">
							<?php
								$posts = get_field('link_service_2');
								if( $posts ): 
									foreach( $posts as $post): ?>
										<a href="<?php the_permalink(); ?>">READ MORE > </a>
									<?php endforeach; 
									wp_reset_postdata();
								endif; ?>
						</div>
					</div>

					<div class="left-box">	
						<div class="service-title">
							<?php the_field('service_title_2'); ?>
						</div>
						<div class="service-details">
							<?php the_field('service_details_2'); ?>
						</div>

						<div class="items-list">
							<div class="left-list">										
								<?php the_field('left_list_items_2'); ?>
							</div>
							<div class="right-list">
								<?php the_field('right_list_items_2'); ?>									
							</div>
						</div>
						<div class="read-more-link">
							<?php
								$posts = get_field('link_service_2');
								if( $posts ): 
									foreach( $posts as $post): ?>
										<a href="<?php the_permalink(); ?>">READ MORE > </a>
									<?php endforeach; 
									wp_reset_postdata();
								endif; ?>
						</div>
					</div>  <!-- .left-box -->
					<div class="right-box">
						<img src="<?php echo get_template_directory_uri(); ?>/img/services_security.png" class="service-icon">
					</div>
				</div> <!-- .service-container.serv2 -->

				<div class="service-container serv3">
					<div class="left-box">
						<img src="<?php echo get_template_directory_uri(); ?>/img/services_cloud.png" class="service-icon" >		
					</div>
					<div class="right-box">
						<div class="service-title">
							<?php the_field('service_title_3'); ?>
						</div>
						<div class="service-details">
							<?php the_field('service_details_3'); ?>
						</div>							
						
						<div class="items-list">
							<div class="left-list">										
								<?php the_field('left_list_items_3'); ?>
							</div>
							<div class="right-list">
								<?php the_field('right_list_items_3'); ?>									
							</div>

							<div class="one-list">				
								<?php the_field('left_list_items_3'); ?>
								<?php the_field('right_list_items_3'); ?>
							</div>							
						</div>
						<div class="read-more-link">
							<?php
								$posts = get_field('link_service_3');
								if( $posts ): 
									foreach( $posts as $post): ?>
										<a href="<?php the_permalink(); ?>">READ MORE > </a>
									<?php endforeach; 
									wp_reset_postdata();
								endif; ?>
						</div>

					</div> <!-- .right-box -->
				</div> <!-- .service-container.serv3 -->

				<div class="service-container serv4">
					<div class="mobile-style">
						<div class="img-box">
							<img src="<?php echo get_template_directory_uri(); ?>/img/services_voice.png" class="service-icon">
						</div>
						<div class="service-title">
							<?php the_field('service_title_4'); ?>
						</div>
						<div class="service-details">
							<?php the_field('service_details_4'); ?>
						</div>

						<div class="items-list">
							<div class="one-list">				
								<?php the_field('left_list_items_4'); ?>
								<?php the_field('right_list_items_4'); ?>
							</div>
							<div class="left-list">										
								<?php the_field('left_list_items_4'); ?>
							</div>
							<div class="right-list">
								<?php the_field('right_list_items_4'); ?>									
							</div>
						</div>
						<div class="read-more-link">
							<?php
								$posts = get_field('link_service_4');
								if( $posts ): 
									foreach( $posts as $post): ?>
										<a href="<?php the_permalink(); ?>">READ MORE > </a>
									<?php endforeach; 
									wp_reset_postdata();
								endif; ?>
						</div>
					</div>

					<div class="left-box">	
						<div class="service-title">
							<?php the_field('service_title_4'); ?>
						</div>
						<div class="service-details">
							<?php the_field('service_details_4'); ?>
						</div>

						<div class="items-list">
							<div class="left-list">										
								<?php the_field('left_list_items_4'); ?>
							</div>
							<div class="right-list">
								<?php the_field('right_list_items_4'); ?>									
							</div>
						</div>
						<div class="read-more-link">
							<?php
								$posts = get_field('link_service_4');
								if( $posts ): 
									foreach( $posts as $post): ?>
										<a href="<?php the_permalink(); ?>">READ MORE > </a>
									<?php endforeach; 
									wp_reset_postdata();
								endif; ?>
						</div>
					</div>  <!-- .left-box -->
					<div class="right-box">
						<img src="<?php echo get_template_directory_uri(); ?>/img/services_voice.png" class="service-icon">
					</div>
				</div> <!-- .service-container.serv4 -->

			</div> <!-- .container -->
			
		</div> <!-- .general-services-section.page-section -->

		<?php endwhile; ?>

		<?php  get_footer(); ?>

	</div> <!-- .about-us-page -->
	