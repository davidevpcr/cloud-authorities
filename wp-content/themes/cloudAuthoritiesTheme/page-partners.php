<?php get_header(); ?>


<?php while(have_posts()): the_post(); ?>
	<div class="partners-page">
		<header class="header page-section">
			<div class="container">
				<div class="top-menu">
					<div class="logo">
						<a href="<?php echo esc_url(home_url('/')); ?>">
							<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" >
						</a>
					</div> <!-- .logo -->
					
					<div class="mobile-menu">
						<a href="#" class="mobile"><i class="fa fa-bars" arial-hidden="true"></i> Menu</a>
					</div>

					<div class="header-menu">
						<?php $args = array(
							'theme_location' => 'header-menu',
							'container' => 'nav',
							'container_class' => 'header-menu-items',
							'container_id' => 'header-menu-items'
							);
							wp_nav_menu($args);
						?>
					</div> <!-- .header-info -->
				</div> <!-- .top-menu -->

				<div class="header-content">
					<div class="left">
						<?php the_content(); ?>
					</div>

					<div class="right">
						<?php echo do_shortcode( '[contact-form-7 id="91" title="Partner With Us"]' ); ?>
					</div>

				</div>
			</div> <!-- .container -->
		</header>

		<div class="service-section page-section">
			<div class="container">		
				<?php
					$c = 1;
					while($c <= 3) { 
						$id_image = get_field('service_icon_' . $c);
						$image = wp_get_attachment_image_src($id_image);					
				?>
					<div class="service-container">
					
						<img src="<?php echo $image[0];?>" class="service-icon" >				

						<div class="service-title">
							<?php the_field('service_title_' . $c); ?>
						</div>
						<div class="service-description">
							<?php the_field('service_description_' . $c); ?>
						</div>	
					</div> <!-- .description-container <?php echo $c ; ?> -->

				<?php $c = $c + 1;} ?>
			</div> <!-- .container -->
		</div> <!-- .service-section.page-section -->


		<div class="trust-section page-section">
			<div class="container">
				<div class="trust-description">
					<?php the_field('carriers_description'); ?>
				</div>
				<div class="trust-logos">
					<?php
						$c = 1;
						while($c <= 6) {
							$id_image = get_field('carrier_logo_' . $c);
							$image = wp_get_attachment_image_src($id_image);
							$c = $c + 1;
					?>
						<img src="<?php echo $image[0];?>" class="trust-icon" >
					<?php } ?>
				</div> <!-- .trust-logos -->
			</div> <!-- .container -->
		</div> <!-- .trust-home -->


		<div class="testimonials-section page-section">
			<div class="container">
				<?php
					$c = 1;
					while($c <= 3) { 					
				?>
					<div class="testimonial-container">					
						<div class="testimonial-title">
							<?php the_field('testimonial_title_' . $c); ?>
						</div>
						<div class="testimonial-description">
							<?php the_field('testimonial_description_' . $c); ?>
						</div>	
					</div> <!-- .description-container <?php echo $c ; ?> -->
				<?php $c = $c + 1; } ?>
			</div>
		</div>


		<div class="about-us-title page-section">
			<div class="container"> 
				<?php the_field('about_us_title'); ?>
			</div>
		</div> <!-- .about-us-title.page-section -->


		<div class="about-us-section page-section">			
			<div class="container">
				<div class="left">
					<?php 
						$videoID = the_field('video_id'); 
						echo $videoID;
						$videoCode = '[video_lightbox_youtube video_id="'. '" width="640" height="480" anchor="212121212121"]';
						echo $videoCode; 
						//echo do_shortcode(  );
					?>					
				</div>
				<div class="right">
					<?php the_field('about_us_text_description'); ?>					
				</div>
			</div>
		</div> <!-- .about-us-section.page-section -->


		<div class="about-us-links-section page-section">
			<div class="info-box"> 
				<div class="about-us-links">
					<?php
						the_field('about_us_link_1');
						the_field('about_us_link_2');
					?>
				</div> <!-- .about-us .about-us-links -->
			</div>
		</div> <!-- .about-us-title.page-section -->

		<div class="call-us-box page-section">
			<div class="container">
				<?php the_field('call_us'); ?>
				
			</div>
		</div>
		<?php endwhile; ?>

		<?php get_footer(); ?>

	</div> <!-- .about-us-page -->