<?php get_header(); ?>


<?php while(have_posts()): the_post(); ?>
	<div class="about-us-page">
		<header class="header page-section">
			<div class="container">
				<div class="top-menu">
					<div class="logo">
						<a href="<?php echo esc_url(home_url('/')); ?>">
							<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" >
						</a>
					</div> <!-- .logo -->
					
					<div class="mobile-menu">
						<a href="#" class="mobile"><i class="fa fa-bars" arial-hidden="true"></i> Menu</a>
					</div>

					<div class="header-menu">
						<?php $args = array(
							'theme_location' => 'header-menu',
							'container' => 'nav',
							'container_class' => 'header-menu-items',
							'container_id' => 'header-menu-items'
							);
							wp_nav_menu($args);
						?>
					</div> <!-- .header-info -->
				</div> <!-- .top-menu -->

				<div class="title">
					<?php the_title( '<h1>', '</h1>'); ?>
				</div>

			</div> <!-- .container -->
		</header>


		<div class="top_about page-section">
			<div class="container">
				<div class="left">
					<div class="left_title">
						<?php the_field('left_title'); ?>					
					</div> 
					<div class="left_text">
						<?php the_field('left_text'); ?>						
					</div>
				</div>
				<div class="right">
					<?php the_content(); ?>
				</div>
			</div>
		</div>


		<div class="central_about page-section">
			<div class="central_image_box">
				<img src="<?php echo get_field('central_image'); ?>" class="central_image" >
			</div>

			<div class="container">		
				<div class="central_description_box">
					<?php the_field('central_description'); ?>
				</div>
			</div>	
		</div>

		<div class="bottom-descriptions page-section">
			<div class="container">		
				<?php
					$c = 1;
					while($c <= 4) { 
						$id_image = get_field('icon_' . $c);
						$image = wp_get_attachment_image_src($id_image);					
				?>

					<div class="description-container">
					
						<img src="<?php echo $image[0];?>" class="description-icon" >				

						<div class="description-title">
							<?php the_field('bottom_title_' . $c); ?>
						</div>
						<div class="description-text">
							<?php the_field('bottom_description_' . $c); ?>
						</div>	
					</div> <!-- .description-container <?php echo $c ; ?> -->

				<?php $c = $c + 1;} ?>	
				
			</div> <!-- .container -->
		</div> <!-- .bottom-descriptions -->

		<div class="bottom-stats page-section">
			<div class="container">		
				<?php
					$c = 1;
					while($c <= 4) { ?>

					<div class="statistic-container">
						<div class="statistic-number">
							<?php the_field('bottom_statistic_number_' . $c); ?>
						</div>
						<div class="statistic-title">
							<?php the_field('bottom_statistic_title_' . $c); ?>
						</div>	
					</div> <!-- service <?php echo $c ; ?> -->

				<?php $c = $c + 1;} ?>	
				
			</div> <!-- .container -->
		</div> <!-- .services-home -->


		<?php endwhile; ?>

		<?php get_footer(); ?>

	</div> <!-- .about-us-page -->