$ = jQuery.noConflict();

$(document).ready(function() {

  // Ocultar y Mostrar menu
  $('.select-type-box select').append($('<option>', {
    value: '000',
    text: 'Service Type',
    selected: true,
  }));

  $("select option[value='000']").attr("disabled", "true").attr("hidden", "true");


  // Ocultar y Mostrar menu
  $('.mobile-menu a').on('click', function() {
    $('nav.header-menu-items').toggle('slow');

    if ($('header .header-menu').hasClass("mobile")) {
      $("header .header-menu").attr("class", "header-menu");
    } else {
      $("header .header-menu").attr("class", "header-menu mobile");
    }

  });

  ajustsByResize();

  $(window).resize(function() {
    ajustsByResize();
  });

});

function ajustsByResize() {
    var breakpoint = 768;

    if($("body").width() >= breakpoint) {
      showMenu();
      if ($(".about-us-page").length) {
        resetHeightBackground();
      }
    } else {
      hideMenu();
      if ($(".about-us-page").length) {
        ajustHeightBackground(0.47);
      }       
    }

    if ($(".about-us-page").length) {
      if($("body").width() <= 460) {
        ajustHeightBackground(0.44);          
      } else if($("body").width() <= 380) {
          ajustHeightBackground(0.42);
      } else if($("body").width() <= 310) {
          ajustHeightBackground(0.4);
      }  
    }

    $("header .header-menu").attr("class", "header-menu");
}

function showMenu() {
  $('nav.header-menu-items').show();
  $("header .header-menu").attr("class", "header-menu");
}

function hideMenu() {
  $('nav.header-menu-items').hide();  
}

function ajustHeightBackground(ajust) {
  $(".central_image_box").height($("body").width() * ajust);
}

function resetHeightBackground() {
  $(".central_image_box").height("");
}





