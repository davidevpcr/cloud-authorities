

		

		<div class="footer page-section">

			<div class="container">
				<img src="<?php echo get_template_directory_uri(); ?>/img/logo_footer.png" >

				<div class="footer-menu">
					<?php $args = array(
						'theme_location' => 'header-menu',
						'container' => 'nav',
						'container_class' => 'footer-menu-items',
						'container_id' => 'footer-menu-items'
						);
						wp_nav_menu($args);
					?>
				</div> <!-- .footer-menu -->

				<?php $args = array(
					'theme_location' => 'social-menu',
					'container' => 'nav',
					'container_class' => 'social',
					'container_id' => 'social',
					'link_before' => '<span class="sr-text">',
					'link_after' => '</span>'
					);
					wp_nav_menu($args);
				?>	
			</div>
				

		</div>
		
		
	</body>
</html>
