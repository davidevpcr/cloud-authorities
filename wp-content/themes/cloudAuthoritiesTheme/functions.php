<?php

	function cloudAuthoritiesTheme_styles() {
		wp_register_style('style', get_template_directory_uri() . '/style.css', array(), '1.0');
		wp_register_style('style_about_us', get_template_directory_uri() . '/css/style_about_us.css', array(), '1.0');
		wp_register_style('style_partners', get_template_directory_uri() . '/css/style_partners.css', array(), '1.0');
		wp_register_style('style_general_service', get_template_directory_uri() . '/css/style_general_services.css', array(), '1.0');
		wp_register_style('style_services_detailed', get_template_directory_uri() . '/css/style_services_detailed.css', array(), '1.0');
		
		wp_enqueue_style('style');
		wp_enqueue_style('style_about_us');
		wp_enqueue_style('style_partners');
		wp_enqueue_style('style_general_service');
		wp_enqueue_style('style_services_detailed');
		

		wp_register_style('fontawesome', get_template_directory_uri() . '/css/font-awesome.css', array(), '4.7.0');
		wp_enqueue_style('fontawesome');

		wp_enqueue_script('jquery');
		wp_enqueue_script( 'my-script', get_stylesheet_directory_uri() . '/js/script.js', array('jquery'), true );

	}
	add_action('wp_enqueue_scripts', 'cloudAuthoritiesTheme_styles');



	function cloudAuthoritiesTheme_menus() {
		register_nav_menus(array(
			'header-menu' => __('Header Menu', 'cloudAuthoritiesTheme'),
			'social-menu' => __('Social Menu', 'cloudAuthoritiesTheme')
		));
	}
	add_action('init', 'cloudAuthoritiesTheme_menus');
	


	function cloudAuthoritiesTheme_fonts(){
	    wp_enqueue_script('google-fonts', 'https://fonts.googleapis.com/css?family=Montserrat');
	}
	add_action('wp_enqueue_scripts', 'cloudAuthoritiesTheme_fonts');

